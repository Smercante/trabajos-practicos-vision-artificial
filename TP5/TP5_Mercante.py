import cv2
import numpy as np
img = cv2.imread('UTN.PNG', 0)
h, w = img.shape
img_rastro = img.copy()
drawing = False  # true if mouse is pressed
corte = False
xi, yi, xf, yf, k = -1, -1, -1, -1, -1


def draw(event, x, y, flags, param):
    global xi, yi, xf, yf, drawing, corte
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        xi, yi = x, y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True:
                cv2.rectangle(img_rastro, (xi, yi), (x,y), (0, 255, 0), 2)
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        xf, yf = x, y
        corte = True


cv2.namedWindow('image')
while(1):
    if corte is False:
        cv2.setMouseCallback('image', draw)
        cv2.imshow('image', img_rastro)
        img_rastro = img.copy()
        k = cv2.waitKey(1) & 0xFF
        if k == 113:  # ASCII de 'q'
            break
    if corte is True:
        cv2.destroyWindow('image')
        img_salida = np.zeros((abs(yf-yi), abs(xf-xi)), np.uint8)
        if yi < yf:
            y0 = yi
        else:
            y0 = yf
        if xi < xf:
            x0 = xi
        else:
            x0 = xf
        for i in range(abs(yf - yi)):
            for j in range(abs(xf - xi)):
                img_salida[i, j] = img[y0 + i, x0 + j]
        corte = False
        while(k != 113 and k != 114 and k != 103):
            cv2.imshow('Recorte', img_salida)
            k = cv2.waitKey(0) & 0xFF ##con argumento 0 queda esperando que se presione una tecla
        if k == 113:    #ASCII de 'q'
            break
        if k == 114:  #ASCII 'r'
            k=-1
            cv2.destroyWindow('Recorte')
            cv2.namedWindow('image')
        if k == 103:    #ASCII de 'g'
            cv2.imwrite('resultado.png', img_salida)
            break