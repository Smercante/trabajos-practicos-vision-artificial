import numpy as np
import cv2
import matplotlib.pyplot as plt
import math

MIN_MATCH_COUNT = 10

colon1 = cv2.imread("colon1.png")
colon1 = cv2.cvtColor(colon1, cv2.COLOR_BGR2RGB)
colon1Gray = cv2.cvtColor(colon1, cv2.COLOR_BGR2GRAY)
colon2 = cv2.imread("colon2.png")
colon2 = cv2.cvtColor(colon2, cv2.COLOR_BGR2RGB)
colon2Gray = cv2.cvtColor(colon2, cv2.COLOR_BGR2GRAY)

#SIFT patent expired, has been moved to main repo. Use cv2.SIFT_create() instead cv2.xfeatures2d.SIFT_create().
sift = cv2.SIFT_create(250)

keypoints1, descriptors1 = sift.detectAndCompute(colon1Gray, None)
keypoints2, descriptors2 = sift.detectAndCompute(colon2Gray, None)

colon1KP = cv2.drawKeypoints(colon1, keypoints1, None, color=(0, 255, 0),flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
colon2KP = cv2.drawKeypoints(colon2, keypoints2, None, color=(0, 255, 0),flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
plt.figure()
plt.subplot(1,2,1), plt.imshow(colon1KP) , plt.title('Keypoints "colon1"')
plt.subplot(1,2,2), plt.imshow(colon2KP), plt.title('Keypoints "colon2"')


bf = cv2.BFMatcher(cv2.NORM_L2)
matches = bf.knnMatch(descriptors1,descriptors2,k=2)

good = []
for m,n in matches:
    if m.distance < 0.70*n.distance:
        good.append(m)

colonMatches = cv2.drawMatches(colon1,keypoints1,colon2,keypoints2,good,None,flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
plt.figure()
plt.imshow(colonMatches), plt.title('Matches')


if(len(good) > MIN_MATCH_COUNT):
    src_pts = np.float32([keypoints1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
    dst_pts = np.float32([keypoints2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

    H, mask = cv2.findHomography(dst_pts, src_pts, cv2.RANSAC, 5.0)

    h, w = colon2.shape[:2]
    #Se obtiene ancho exacto de la imagen final trasladando los vertices la imagen original de acuerdo a la matriz H
    corners_bef = np.float32([[0, 0], [w, 0], [w, h], [0, h]]).reshape(-1, 1, 2)
    corners_aft = cv2.perspectiveTransform(corners_bef, H)
    #xmin = math.floor(corners_aft[:, 0, 0].min())
    #ymin = math.floor(corners_aft[:, 0, 1].min())
    xmax = math.ceil(corners_aft[:, 0, 0].max())
    #ymax = math.ceil(corners_aft[:, 0, 1].max())

    colon2Warped = cv2.warpPerspective(colon2, H, (xmax, h))
    #plt.figure(), plt.imshow(colon2Warped)

    alpha = 0.5

    fullColon = np.zeros((h, xmax, 3), dtype=np.uint8)

    for i in range(h):
        for j in range(xmax):
            if (j >= w):
                fullColon[i][j] = colon2Warped[i][j]
            else:
                if (sum(colon2Warped[i][j-1])>1):
                    fullColon[i][j] = colon2Warped[i][j] * alpha + colon1[i][j] * (1 - alpha)
                else:
                    fullColon[i][j] = colon1[i][j]

    cv2.imwrite('fullColon.png', cv2.cvtColor(fullColon, cv2.COLOR_RGB2BGR))
    plt.figure(), plt.imshow(fullColon), plt.title('Teatro Colón completo')
    plt.show()
    
else:
    print("No se han encontrado suficientes coincidencias entre las imagenes.")

