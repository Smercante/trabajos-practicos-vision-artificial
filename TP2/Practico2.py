import cv2

img = cv2.imread ( 'UTN.PNG' , 0 )
h,w = img.shape
thr = 100

#enumerate devuelve el indice del argumento (fila de la imagen) y el valor (color) del mismo
for i, row in enumerate(img):
    for j, col in enumerate(row):
        if (col >= thr):
            img[i,j] = 255
        else:
            img[i,j] = 0

cv2.imwrite ('resultado.png' , img )