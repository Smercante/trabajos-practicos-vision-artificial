import numpy as np
import cv2

imgPerspective = cv2.imread('imgPerspective.jpg')
count=0
points = np.zeros(shape=(2,4))

def SelectPoints(event, x, y, flags, param):
    global count, points
    if event == cv2.EVENT_LBUTTONDOWN:
        points[0][count]=x
        points[1][count]=y
        count = count + 1
        if(count==4):
            cv2.destroyAllWindows()

cv2.namedWindow('imgPerspective')
print('Indique con cuatro clic izquierdos los vertices la imagen a seleccionar,\nde izquierda a derecha y'
      ' de arriba hacia abajo, considerando la posición final del objeto o forma.')
while(count<4):
    cv2.setMouseCallback('imgPerspective', SelectPoints)
    cv2.imshow('imgPerspective', imgPerspective)
    k = cv2.waitKey(1) & 0xFF
    if k == 113:  # ASCII 'q'
        break

#points: Fila 0 es eje horizontal, fila 1 eje vertical. el número de columna es el nro de punto (ver points.jpg)
TopWidth = np.sqrt(((points[0][0] - points[0][1]) ** 2) + ((points[1][0] - points[1][1]) ** 2))
BottomWidth = np.sqrt(((points[0][2] - points[0][3]) ** 2) + ((points[1][2] - points[1][3]) ** 2))
maxWidth = max(int(TopWidth), int(BottomWidth))

LeftHeight = np.sqrt(((points[0][0] - points[0][2]) ** 2) + ((points[1][0] - points[1][2]) ** 2))
RightHeight = np.sqrt(((points[0][1] - points[0][3]) ** 2) + ((points[1][1] - points[1][3]) ** 2))
maxHeight = max(int(LeftHeight), int(RightHeight))

input_pts = np.float32([[points[0][0], points[1][0]],
                        [points[0][1], points[1][1]],
                        [points[0][2], points[1][2]],
                        [points[0][3], points[1][3]]])
output_pts = np.float32([[0, 0],
                        [maxWidth - 1, 0],
                        [0, maxHeight - 1],
                        [maxWidth - 1, maxHeight - 1]])
M = cv2.getPerspectiveTransform(input_pts, output_pts)
imgFlat = cv2.warpPerspective(imgPerspective,M,(maxWidth, maxHeight),flags=cv2.INTER_LINEAR)


cv2.imwrite ('imgFlat.png' , imgFlat)
cv2.imshow('imgPerspective' , imgPerspective)
cv2.imshow('imgFlat' , imgFlat)
print('Presione cualquier tecla para salir. La imagen resultante se ha guardado correctamente.')
cv2.waitKey(0)
cv2.destroyAllWindows()