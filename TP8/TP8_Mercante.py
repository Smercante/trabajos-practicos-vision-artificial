import numpy as np
import cv2

screen = cv2.imread('screen.jpg',0)
Advertising = cv2.imread('advertising.jpg',0)
height, width = screen.shape[:2]
resizeAdv=cv2.resize(Advertising, (width, height))
#cv2.imshow('resizeAdv', resizeAdv)

count=0
points = np.zeros(shape=(2,3))

def SelectPoints(event, x, y, flags, param):
    global count, points
    if event == cv2.EVENT_LBUTTONDOWN:
        points[0][count]=x
        points[1][count]=y
        count = count + 1

cv2.namedWindow('screen')

print('Indique con tres clic izquierdos donde desea insertar la imagen.\nDe izquierda a derecha y de arriba hacia abajo.')
while(count<3):
    cv2.setMouseCallback('screen', SelectPoints)
    cv2.imshow('screen', screen)
    k = cv2.waitKey(1) & 0xFF
    if k == 113:  # ASCII 'q'
        break

input_pts = np.float32([[0, 0], [width - 1, 0], [0, height - 1]])
output_pts = np.float32([[points[0][0], points[1][0]], [points[0][1], points[1][1]], [points[0][2], points[1][2]]])
M = cv2.getAffineTransform(input_pts, output_pts)
affinedAdv = cv2.warpAffine(resizeAdv, M, (width, height))
#cv2.imshow('affinedAdv', affinedAdv)

for i, row in enumerate(affinedAdv):
    for j, col in enumerate(row):
        if (col > 1):
            screen[i,j] = affinedAdv[i,j]

cv2.imwrite ('resul.png' , screen)
cv2.imshow('result' , screen)
print('Presione cualquier tecla para salir. La imagen resultante se ha guardado correctamente.')
cv2.waitKey(0)