import numpy as np
import cv2
import matplotlib.pyplot as plt
import math

img = cv2.imread ('plane.jpg' , 0 )

def TranslateNRotate(img, x, y, degree):
    # shape returns a tuple of the number of rows, columns, and channels (if the image is color). Se usa solo h y w.
    h, w = img.shape[:2]
    rad=degree*2*math.pi/360
    center=(w/2, h/2)

    #Se calcula la matriz de traslacion y rotacion y se las multiplica, para ello se le agrega una tercer fila [0, 0, 1]
    #lo cual se conoce como coorenadas homogeneas.

    Mt = np.float32([[1, 0, x],
                    [0, 1, y],
                    [0, 0, 1]])
    Mr = cv2.getRotationMatrix2D(center, degree, 1.0)
    Mr = np.vstack([Mr, [0, 0, 1]]) #Agrega una fila
    Mtr = np.matmul(Mt, Mr)
    ShiftedNRotated = cv2.warpPerspective(img, Mtr, (w, h)) #warpPerspective is used because matrix is now 3x3 not 3x2
    return ShiftedNRotated

while(1):
    dx = int(input('Ingresar cantidad de pixeles horizontales a desplazar (positivo a la derecha): '))
    if (dx >= abs((img.shape[1]))):
        print('Ingrese un numero de módulo menor a {}'.format(int(img.shape[1])))
    elif(dx < (img.shape[1])):
        break
while(1):
    dy = int(input('Ingresar cantidad de pixeles verticales a desplazar (positivo hacia abajo): '))
    if (dy >= abs((img.shape[0]))):
        print('Ingrese un numero de módulo menor a {}'.format(int(img.shape[0])))
    elif(dy < (img.shape[0])):
        break
degree=int(input('Ingresar angulo de rotación en grados, sentido antihorario: '))

shift=TranslateNRotate(img,dx,dy, degree)

plt.subplot(1,2,1), plt.imshow(img), plt.title('Input')
plt.subplot(1,2,2), plt.imshow(shift), plt.title('Output')
plt.show()