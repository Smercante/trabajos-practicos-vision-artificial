import numpy as np
import cv2

homePerspective = cv2.imread('homePerspective.jpeg')
height, width = homePerspective.shape[:2]
count=0
points = np.zeros(shape=(2,4))

def SelectPoints(event, x, y, flags, param):
    global count, points
    if event == cv2.EVENT_LBUTTONDOWN:
        points[0][count]=x
        points[1][count]=y
        count = count + 1
        if(count==4):
            cv2.destroyAllWindows()

cv2.namedWindow('homePerspective')
print('Indique con clic izquierdo las esquinar del "marco" de la puerta en la pared, de izquierda a derecha y de arriba hacia abajo.')
while(count<4):
    cv2.setMouseCallback('homePerspective', SelectPoints)
    cv2.imshow('homePerspective', homePerspective)
    k = cv2.waitKey(1) & 0xFF
    if k == 113:  # ASCII 'q'
        break


RightHeight = np.sqrt(((points[0][1] - points[0][3]) ** 2) + ((points[1][1] - points[1][3]) ** 2))
HeightWidthRelation=205.5/90.5
cmDivPixel=205.5/float(RightHeight)
realWeight=RightHeight/HeightWidthRelation

input_pts = np.float32([[points[0][0], points[1][0]],
                        [points[0][1], points[1][1]],
                        [points[0][2], points[1][2]],
                        [points[0][3], points[1][3]]])
output_pts = np.float32([[points[0][1]-realWeight, points[1][1]],
                        [points[0][1], points[1][1]],
                        [points[0][3]-realWeight, points[1][3]],
                        [points[0][3], points[1][3]]])
M = cv2.getPerspectiveTransform(input_pts, output_pts)
homeFlat = cv2.warpPerspective(homePerspective,M,(width, height),flags=cv2.INTER_LINEAR)
homeFlatBuffer=homeFlat.copy()


def SelectPoints2(event, x, y, flags, param):
    global count, points
    if event == cv2.EVENT_LBUTTONDOWN:
        points[0][count]=x
        points[1][count]=y
        count = count + 1
cv2.namedWindow('homeFlatBuffer')
print('Haga dos clic para conocer distancias paralelas al plano de la fachada.\n')
print("Luego de cada medición, presione: 'a' para agregar una medición, 'r' para resetear mediciones,"
      " 'g' para guardar, 'q' para descartar todo y salir. ")
count=0
while(1):
    if k==103 or k==113:
        break
    while(count<2):
        cv2.setMouseCallback('homeFlatBuffer', SelectPoints)
        if(count<2):
            cv2.imshow('homeFlatBuffer', homeFlatBuffer)
            k = cv2.waitKey(1) & 0xFF
            if k == 113:  # ASCII 'q'
                break
        if(count==2):
            cv2.destroyAllWindows()
    count=0
    distancePixel = int(np.sqrt(((points[0][0] - points[0][1]) ** 2) + ((points[1][0] - points[1][1]) ** 2)))+2
    distance = round((cmDivPixel * distancePixel/100),2)
    line_thickness = 2
    cv2.line(homeFlatBuffer, (int(points[0][0]), int(points[1][0])), (int(points[0][1]), int(points[1][1])), (0, 255, 0),
             thickness=line_thickness)
    orgX = int(abs(points[0][0] - points[0][1]) / 2) + min(int(points[0][0]), int(points[0][1]))
    orgY = int(abs(points[1][0] - points[1][1]) / 2) + min(int(points[1][0]), int(points[1][1]))
    cv2.putText(homeFlatBuffer, str(distance), (orgX, orgY), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8, (0, 0, 0))
    while (k != 113 and k != 114 and k != 103 and k != 97):
        cv2.imshow('homeFlatBuffer', homeFlatBuffer)
        k = cv2.waitKey(0) & 0xFF
        if k == 113:  # ASCII de 'q'
            cv2.destroyWindow('homeFlatBuffer')
            break
        if k == 114:  # ASCII 'r'
            cv2.destroyWindow('homeFlatBuffer')
            cv2.namedWindow('homeFlatBuffer')
            homeFlatBuffer=homeFlat.copy()
        if k == 97:  # ASCII 'a'
            cv2.destroyWindow('homeFlatBuffer')
            cv2.namedWindow('homeFlatBuffer')
        if k == 103:  # ASCII de 'g'
            cv2.imwrite('homeFlat.png', homeFlatBuffer)
            cv2.destroyWindow('homeFlatBuffer')
            break