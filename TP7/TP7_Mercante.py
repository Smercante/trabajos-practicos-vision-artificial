import numpy as np
import cv2
import matplotlib.pyplot as plt
import math

img = cv2.imread ('plane.jpg' , 0 )

def TranslateNRotateNScaled(img, x, y, degree, s):
    # shape returns a tuple of the number of rows, columns, and channels (if the image is color). Se usa solo h y w.
    h, w = img.shape[:2]
    rad=degree*2*math.pi/360
    Mtr = np.float32([[s*math.cos(rad), s*math.sin(rad), x],
                     [-s*math.sin(rad), s*math.cos(rad), y]])
    ShiftedNRotated=cv2.warpAffine(img, Mtr, (w, h))
    return ShiftedNRotated

while(1):
    dx = int(input('Ingresar cantidad de pixeles horizontales a desplazar (positivo a la derecha): '))
    if (dx >= abs((img.shape[1]))):
        print('Ingrese un numero de módulo menor a {}'.format(int(img.shape[1])))
    elif(dx < (img.shape[1])):
        break
while(1):
    dy = int(input('Ingresar cantidad de pixeles verticales a desplazar (positivo hacia abajo): '))
    if (dy >= abs((img.shape[0]))):
        print('Ingrese un numero de módulo menor a {}'.format(int(img.shape[0])))
    elif(dy < (img.shape[0])):
        break
degree=int(input('Ingresar angulo de rotación en grados, sentido antihorario: '))
while(1):
    scale = float(input('Ingresar factor de escalado de módulo mayor a 0.0 y menor a 3.0: '))
    if ((3.0<abs(scale)) or (0.0==abs(scale))):
        print('Valor ingresado fuera del rango solicitado')
    else:
        break

TRS=TranslateNRotateNScaled(img,dx,dy, degree, scale)

plt.subplot(1,2,1), plt.imshow(img), plt.title('Input')
plt.subplot(1,2,2), plt.imshow(TRS), plt.title('Output')
plt.show()